<?php
    // set script name and page title
    $g_ScriptName = strtolower(basename($_SERVER['PHP_SELF']));
    $g_IsPageHeaderVisible = true;
    $g_PageHeaderTitle = 'Gigs';

    include_once("_appcode/CDao.php");

    // default parameters
    $currCategoryID = 0;
    $currPage = 1;
    $pageSize = 9;

    // get the QS params
    $qsCategoryID = $_GET["catid"];
    $qsPage = $_GET["p"];

    if (is_numeric($qsCategoryID))
    {
        $currCategoryID = intval($qsCategoryID);
    }
    if (is_numeric($qsPage))
    {
        $currPage = intval($qsPage);
    }

    // get info from database
    $db = new CDao();
    $categories = $db->GetCategories();

    $jobData = $db->GetJobs($currCategoryID, $currPage, $pageSize);
    $jobs = $jobData["Jobs"];

    // pager data
    $pagerData = [
            "TotalJobCount" => $jobData["TotalJobCount"],
            "LastPage" => (intval($jobData["TotalJobCount"] / $pageSize) + 1),
            "PrevPage" => ($currPage - 1),
            "NextPage" => ($currPage + 1)
        ];
    $pagerData["StartJobNumber"] = ((($currPage - 1) * $pageSize) + 1);
    $pagerData["EndJobNumber"] = ($pagerData["StartJobNumber"] + count($jobs) - 1);
    $pagerData["IsPrevPageAvailable"] = ($currPage > 1);
    $pagerData["IsNextPageAvailable"] = ($currPage < $pagerData["LastPage"]);
    $pagerData["FirstPageUrl"] = "newgigs.php?catid={$currCategoryID}";
    $pagerData["PrevPageUrl"] = "newgigs.php?catid={$currCategoryID}&p={$pagerData["PrevPage"]}";
    $pagerData["NextPageUrl"] = "newgigs.php?catid={$currCategoryID}&p={$pagerData["NextPage"]}";
    $pagerData["LastPageUrl"] = "newgigs.php?catid={$currCategoryID}&p={$pagerData["LastPage"]}";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WHO... a staffing company | Gigs</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Abel|Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
        <link href="css/jqModal.css" rel="stylesheet">
        <link href="css/new.css" rel="stylesheet">
        <link href="css/new-mobile.css" rel="stylesheet">

        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.sticky-kit.min.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-77414345-1', 'auto');
            ga('send', 'pageview');
        </script>

    </head>
    <body>
        <div class="csi-page-container">

            <!-- top navigation bar -->
            <!--<?php include('_appcode/mod/nav-header.php') ?>-->
            <div id="topNavBar">
              <div class="visible-md-block visible-lg-block csi-navbar-title">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="csi-navbar-title-content text-center">
                        <a href="/"><img src="/assets/svg/who-logo-mark.svg"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- job listings -->
            <div class="csi-contentpane">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-6 col-md-offset-1">
                        <!-- check for jobs in category -->
                        <?php if (count($jobs) > 0) { ?>
                            <h2 class="csi-job-list-title">Job Listings</h2>
                            <div id="jobList">
                                <?php foreach($jobs as $job) { ?>
                                    <?php $postDate = strtotime($job["PostDate"]) ?>
                                    <div class="csi-job">
                                        <h2><?php echo $job["Title"]; ?></h2>
                                        <div class="csi-job-listdate">
                                          <span class="csi-job-listdate-date"><?php echo date("d", $postDate) ?></span>
                                          <span class="csi-job-listdate-monthyear">
                                            <?php echo date("M", $postDate); ?>
                                            <?php echo date("Y", $postDate); ?>
                                          </span>
                                        </div>
                                        <h3><?php echo $job["Location"]; ?></h3>
                                        <p><?php echo $job["Summary"]; ?></p>
                                        <h4>Job Description</h4>
                                        <p><?php echo $job["Description"]; ?></p>
                                        <div class="csi-job-action">
                                          <a class="" href="newgig.php?jobid=<?php echo $job["ID"]; ?>">Read More</a>
                                          <a class="" href="mailto:resumes@whostaffing.com?subject=Application For Job ID <?php echo $job["ID"]; ?>">Apply Here</a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="csi-job-pager">
                                <span><?php echo $pagerData["StartJobNumber"]; ?>-<?php echo $pagerData["EndJobNumber"]; ?>/<?php echo $pagerData["TotalJobCount"]; ?></span>
                                <a class="<?php echo ($pagerData["IsPrevPageAvailable"] ? "" : "disabled"); ?>" href="<?php echo ($pagerData["IsPrevPageAvailable"] ? $pagerData["FirstPageUrl"] : "javascript:void(0)"); ?>" title="First Page"><span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-left"></a>
                                <a class="<?php echo ($pagerData["IsPrevPageAvailable"] ? "" : "disabled"); ?>" href="<?php echo ($pagerData["IsPrevPageAvailable"] ? $pagerData["PrevPageUrl"] : "javascript:void(0)"); ?>" title="Previous Page"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="<?php echo ($pagerData["IsNextPageAvailable"] ? "" : "disabled"); ?>" href="<?php echo ($pagerData["IsNextPageAvailable"] ? $pagerData["NextPageUrl"] : "javascript:void(0)"); ?>" title="Next Page"><span class="glyphicon glyphicon-chevron-right"></a>
                                <a class="<?php echo ($pagerData["IsNextPageAvailable"] ? "" : "disabled"); ?>" href="<?php echo ($pagerData["IsNextPageAvailable"] ? $pagerData["LastPageUrl"] : "javascript:void(0)"); ?>" title="Last Page"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                            <!-- no jobs in category -->
                            <?php } else { ?>
                            <p class="perfect-role">Didn’t find your perfect role? We’d still like to hear from you! <a href="mailto:info@whostaffing.com">Drop us a line</a> to be the first to hear about new opportunities.</p>
                            <?php } ?>
                        </div>
                        <div class="hidden-xs col-sm-4 col-md-4 col-md-offset-1">
                            <h2>Filter By Skills</h2>
                            <ul class="csi-job-filters">
                                <li class="<?php if ($currCategoryID == 0) { echo "selected"; } ?>">
                                    <a href="newgigs.php">All Listings</a>
                                </li>
                                <?php foreach($categories as $category) { ?>
                                    <li class="<?php if ($category["ID"] == $currCategoryID) { echo "selected"; } ?>">
                                        <a href="newgigs.php?catid=<?php echo $category["ID"]; ?>"><?php echo $category["Name"]; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- footer -->
            <?php include('_appcode/mod/footer.php') ?>
        </div>

        <!-- work with us modal -->
        <?php include('_appcode/mod/modal-workwithus.php') ?>

        <!-- scripts -->
        <script>
          var re = new RegExp(String.fromCharCode(160), "g");
          $("p").each(function(){
            var str = $(this).text();
            if (str.indexOf("·") == 0) {
              $(this).text(str.replace(re, " "));
            }
          })
        </script>
    </body>
</html>
