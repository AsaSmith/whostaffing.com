<?php
    // set script name and page title
    $g_ScriptName = strtolower(basename($_SERVER['PHP_SELF']));
    $g_IsPageHeaderVisible = true;
    $g_PageHeaderTitle = 'Gig';

    include_once("_appcode/CDao.php");

    $db = new CDao();

    $jobID = intval($_GET["jobid"]);
    $job = $db->GetJob($jobID);
    $postDate = strtotime($job["PostDate"]);

    $categories = $db->GetCategories();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WHO... a staffing company | Gigs</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Abel|Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
        <link href="css/jqModal.css" rel="stylesheet">
        <link href="css/new.css" rel="stylesheet">
        <link href="css/new-mobile.css" rel="stylesheet">

        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.sticky-kit.min.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-77414345-1', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div class="csi-page-container">

          <!-- top navigation bar -->
          <!--<?php include('_appcode/mod/nav-header.php') ?>-->
          <div id="topNavBar">
            <div class="visible-md-block visible-lg-block csi-navbar-title">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="csi-navbar-title-content text-center">
                      <a href="/"><img src="/assets/svg/who-logo-mark.svg"></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <!-- job listing -->
            <div class="csi-contentpane">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-6 col-md-offset-1">
                            <h2 class="csi-job-list-title"><a href="newgigs.php"><span class="glyphicon glyphicon-triangle-left"></span> Back to Job Listings</a></h2>
                            <div class="csi-job">
                                <h2><?php echo $job["Title"]; ?></h2>
                                <div class="csi-job-listdate">
                                  <span class="csi-job-listdate-date"><?php echo date("d", $postDate) ?></span>
                                  <span class="csi-job-listdate-monthyear">
                                    <?php echo date("M", $postDate); ?>
                                    <?php echo date("Y", $postDate); ?>
                                  </span>
                                </div>
                                <h3><?php echo $job["Location"]; ?></h3>
                                <p><?php echo $job["Summary"]; ?></p>
                                <h4>Job Description</h4>
                                <?php echo $job["Description"]; ?>
                                <h4>Required Skills</h4>
                                <?php echo $job["Skills"]; ?>
                                <h4>Additional Details</h4>
                                <?php echo $job["AdditionalDetails"]; ?>
                                <div class="csi-job-action">
                                  <a class="" href="mailto:resumes@whostaffing.com?subject=Application For Job ID <?php echo $job["ID"]; ?>">Apply to this job</a>
                                </div>
                                <br>
                                <p>Apply to this job by emailing resumes@whostaffing.com with a cover letter &amp; resume.</p>
                            </div>
                        </div>
                        <div class="hidden-xs col-sm-4 col-md-4 col-md-offset-1">
                            <h2>Filter By Skills</h2>
                            <ul class="csi-job-filters">
                                <li><a href="newgigs.php">All Listings</a></li>
                                <?php foreach($categories as $category) { ?>
                                    <li class="<?php if ($category["ID"] == $job["CategoryID"]) { echo "selected"; } ?>">
                                        <a href="newgigs.php?catid=<?php echo $category["ID"]; ?>"><?php echo $category["Name"]; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- footer -->
            <?php include('_appcode/mod/footer.php') ?>
        </div>

        <!-- work with us modal -->
        <?php include('_appcode/mod/modal-workwithus.php') ?>

        <!-- scripts -->
        <script>
          var re = new RegExp(String.fromCharCode(160), "g");
          $("p").each(function(){
            var str = $(this).text();
            if (str.indexOf("·") == 0) {
              $(this).text(str.replace(re, " "));
            }
          })
        </script>
    </body>
</html>
